﻿using JobBoard.Services.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using JobBoard.Models;
using JobBoard.Models.ViewModels;
using JobBoard.Services.Interfaces;

namespace UnitTestJobBoard
{
    [TestClass]
    public class CategoryTest
    {
        [TestMethod]
        public void GetAll_AllCategories_ReturnAllCategories()
        {
            //Arrange
            var admin = new AdminRepository();
            var categoryViewModel = new CategoryViewModel()
            {
                CategoryName = "Banking & Finance",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
            //Act
            var result = admin.GetAll();
            //Assert
            
        }
    }
}
