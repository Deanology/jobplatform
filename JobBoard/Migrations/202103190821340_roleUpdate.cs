namespace JobBoard.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class roleUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetRoles", "CreatedAt", c => c.DateTime());
            AddColumn("dbo.AspNetRoles", "UpDatedAt", c => c.DateTime());
            AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetRoles", "Discriminator");
            DropColumn("dbo.AspNetRoles", "UpDatedAt");
            DropColumn("dbo.AspNetRoles", "CreatedAt");
        }
    }
}
