namespace JobBoard.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullabledate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "UpdatedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Categories", "CreatedAt", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Categories", "CreatedAt", c => c.DateTime());
            AlterColumn("dbo.Categories", "UpdatedAt", c => c.DateTime());
        }
    }
}
