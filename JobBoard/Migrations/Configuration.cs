using JobBoard.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace JobBoard.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<JobBoard.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "JobBoard.Models.ApplicationDbContext";
        }

        protected override void Seed(JobBoard.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(context));

            string userName = "admin@gmail.com";
            string password = "QWerty55!";
            string employerUsername = "employer@gmail.com";
            string employerPassword = "ZXcvbn55!";


            if (!roleManager.RoleExists(RoleNames.ROLE_ADMINISTRATOR))
            {
                var roleresult = roleManager.Create(new ApplicationRole(RoleNames.ROLE_ADMINISTRATOR));
            } 
            if (!roleManager.RoleExists(RoleNames.ROLE_CONTRIBUTOR))
            {
                var roleresult = roleManager.Create(new ApplicationRole(RoleNames.ROLE_CONTRIBUTOR));
            }
            if (!roleManager.RoleExists(RoleNames.ROLE_READER))
            {
                var roleresult = roleManager.Create(new ApplicationRole(RoleNames.ROLE_READER));
            }
            //seed system admin
            ApplicationUser user = userManager.FindByName(userName);
            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = userName,
                    Email = userName,
                    EmailConfirmed = true
                };
                IdentityResult userResult = userManager.Create(user, password);
                if (userResult.Succeeded)
                {
                    var result = userManager.AddToRole(user.Id, RoleNames.ROLE_ADMINISTRATOR);
                }
            }
            //seed employer
            ApplicationUser employer = userManager.FindByName(employerUsername);
            if (employer == null)
            {
                employer = new ApplicationUser()
                {
                    UserName = employerUsername,
                    Email = employerUsername,
                    EmailConfirmed = true
                };
                IdentityResult userResult = userManager.Create(employer, employerPassword);
                if (userResult.Succeeded)
                {
                    var result = userManager.AddToRole(employer.Id, RoleNames.ROLE_CONTRIBUTOR);
                }
            }

        }
    }
}
