namespace JobBoard.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendingRole : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetRoles", "CreatedAt");
            DropColumn("dbo.AspNetRoles", "UpDatedAt");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetRoles", "UpDatedAt", c => c.DateTime());
            AddColumn("dbo.AspNetRoles", "CreatedAt", c => c.DateTime());
        }
    }
}
