﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using JobBoard.Models;
using JobBoard.Models.ViewModels;

namespace JobBoard.Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Category, CategoryViewModel>().ForSourceMember(x => x.UpdatedAt, y => y.Ignore())
                .ForSourceMember(a => a.CreatedAt, b => b.Ignore()).ReverseMap();
        }
    }
}