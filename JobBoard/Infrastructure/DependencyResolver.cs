﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JobBoard.Models;
using JobBoard.Services.Interfaces;
using JobBoard.Services.Repositories;
using Ninject;
using Ninject.Web.Common;

namespace JobBoard.Infrastructure
{
    public class DependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public DependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            _kernel.Bind(typeof(ApplicationDbContext)).ToSelf().InRequestScope();
            _kernel.Bind<IAdminRepository>().To<AdminRepository>();
            _kernel.Bind<IRoleRepository>().To<RoleRepository>();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }
    }
}