﻿using System.Web;
using System.Web.Optimization;

namespace JobBoard
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/unobtrusive").Include(
                "~/Scripts/jquery.unobtrusive-ajax.min.js",
                    "~/Scripts/jquery.validate.unobtrusive.min.js"
                ));
            bundles.Add(new ScriptBundle("~/Assets/jqueryval").Include(
                        "~/Static/Home/assets/js/jquery.min.js",
                        "~/Static/Home/assets/js/popper.min.js",
                        "~/Static/Home/assets/js/bootstrap.min.js",
                        "~/Static/Home/assets/js/select2.min.js",
                        "~/Static/Home/assets/js/aos.js",
                        "~/Static/Home/assets/js/perfect-scrollbar.jquery.min.js",
                        "~/Static/Home/assets/js/owl.carousel.min.js",
                        "~/Static/Home/assets/js/jquery.counterup.min.js",
                        "~/Static/Home/assets/js/slick.js",
                        "~/Static/Home/assets/js/bootstrap-datepicker.js",
                        "~/Static/Home/assets/js/isotope.min.js",
                        "~/Static/Home/assets/js/summernote.js",
                        "~/Static/Home/assets/js/jQuery.style.switcher.js",
                        "~/Static/Home/assets/js/counterup.min.js",
                        "~/Static/Home/assets/js/custom.js"
                        ));
            bundles.Add(new ScriptBundle("~/Admin/jqueryval").Include(
                "~/Static/Admin/assets/js/jquery.min.js",
                "~/Static/Admin/assets/js/popper.min.js",
                "~/Static/Admin/assets/js/bootstrap.min.js",
                "~/Static/Admin/assets/js/index2.js",
                "~/Static/Admin/assets/js/app.js",
                "~/Static/Admin/assets/js/pace.min.js",
                "~/Static/Admin/assets/js/bs-custom-file-input.min.js",
                "~/Static/Admin/assets/plugins/simplebar/js/simplebar.min.js",
                "~/Static/Admin/assets/plugins/metismenu/js/metisMenu.min.js",
                "~/Static/Admin/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js",
                "~/Static/Admin/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js",
                "~/Static/Admin/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js",
                "~/Static/Admin/assets/plugins/vectormap/jquery-jvectormap-in-mill.js",
                "~/Static/Admin/assets/plugins/vectormap/jquery-jvectormap-us-aea-en.js",
                "~/Static/Admin/assets/plugins/vectormap/jquery-jvectormap-uk-mill-en.js",
                "~/Static/Admin/assets/plugins/vectormap/jquery-jvectormap-au-mill.js",
                "~/Static/Admin/assets/plugins/apexcharts-bundle/js/apexcharts.min.js",
                "~/Static/Admin/assets/plugins/datatable/js/jquery.dataTables.min.js"
            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Assets/css").Include(
                      "~/Static/Home/assets/css/plugins.css",
                      "~/Static/Home/assets/css/styles.css",
                      "~/Static/Home/assets/css/skin/default.css"));
            bundles.Add(new StyleBundle("~/Admin/css").Include(
                "~/Static/Admin/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css",
                "~/Static/Admin/assets/plugins/simplebar/css/simplebar.css",
                "~/Static/Admin/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css",
                "~/Static/Admin/assets/plugins/metismenu/css/metisMenu.min.css",
                "~/Static/Admin/assets/css/pace.min.css",
                "~/Static/Admin/assets/css/bootstrap.min.css",
                "~/Static/Admin/assets/css/icons.css",
                "~/Static/Admin/assets/css/app.css",
                "~/Static/Admin/assets/css/dark-sidebar.css",
                "~/Static/Admin/assets/css/dark-theme.css",
                "~/Static/Admin/assets/plugins/datatable/css/dataTables.bootstrap4.min.css",
                "~/Static/Admin/assets/plugins/datatable/css/buttons.bootstrap4.min.css"));
        }
    }
}
