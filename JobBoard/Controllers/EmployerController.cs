﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JobBoard.Models;

namespace JobBoard.Controllers
{
    [Authorize(Roles = "Employer")]
    public class EmployerController : Controller
    {
        // GET: Employer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ChangePassword()
        {
            var model = new ChangePasswordViewModel();
            return PartialView("_ChangePasswordPartial", model);
        }
    }
}