﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using JobBoard.Models;
using JobBoard.Models.ViewModels;
using JobBoard.Services.Interfaces;


namespace JobBoard.Controllers
{
    /*[Authorize(Roles= "Administrator")]*/
    public class AdminController : Controller
    {
        private readonly IAdminRepository _adminRepository;
        private readonly IRoleRepository _roleRepository;
        public AdminController(IAdminRepository adminRepository, IRoleRepository roleRepository)
        {
            _adminRepository = adminRepository;
            _roleRepository = roleRepository;
        }
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Lockout()
        {
            return View();
        }
        //Category Controller
        public ActionResult AllCategories()
        {
            var categoriesDb = _adminRepository.GetAll();
            var categories = Mapper.Map<IEnumerable<CategoryViewModel>>(categoriesDb);
            return View(categories);
        }

        public ActionResult AllRoles()
        {
            var roles = _roleRepository.GetAllRoles();
            return View(roles);
        }

        public ActionResult GetCategory(int id)
        {
            var categoryDb = _adminRepository.GetCategoryById(id);
            var category = Mapper.Map<CategoryViewModel>(categoryDb);
            return View(category);
        }

        public ActionResult AddCategory()
        {
            var model = new CategoryViewModel();
            return View(model);
        }

        public ActionResult AddRole()
        {
            return PartialView("_AddRolePartial");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddRole(ApplicationRoleViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("AddCategory", model);
            }
            var applicationRoleDb = Mapper.Map<ApplicationRole>(model);
            var result = _roleRepository.AddRole(applicationRoleDb);
            //add task to perform 1
            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> EditCategory(int id)
        {
            var categoryDb =await _adminRepository.GetCategoryById(id);
            if (categoryDb == null) return HttpNotFound();
            var category = Mapper.Map<Category, CategoryViewModel>(categoryDb);
            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCategory(CategoryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("AddCategory", model);
            }
            var categoryDb = Mapper.Map<Category>(model);
            _adminRepository.AddCategory(categoryDb);
            TempData["success"] = "Category Successfully Added!";
            return RedirectToAction("AddCategory", "Admin");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateCategory(CategoryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("AddCategory", model);
            }

            var categoryDb =await _adminRepository.GetCategoryById(model.Id);
            var category = Mapper.Map(model, categoryDb);
            category.UpdatedAt = DateTime.Now;
            _adminRepository.UpdateCategory(categoryDb);
            TempData["success"] = "Category Successfully Edited!";
            ModelState.Clear();
            return RedirectToAction("EditCategory", "Admin");
        }
        //GET: Category/Delete/2
        public ActionResult GetDeletePartialView(int id)
        {
            var categoryDb = _adminRepository.GetCategoryById(id);
            var category = Mapper.Map<CategoryViewModel>(categoryDb);
            if (category == null)
                return HttpNotFound();
            return PartialView("_DeleteCategory", category);
        }
        //POST: Category/Delete/2
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var result =_adminRepository.Delete(id);
                return result == Task.CompletedTask
                    ? Json(new {success = true}, JsonRequestBehavior.AllowGet)
                    : Json(new {failure = true, errorMessage = "Category not successfully deleted"},
                        JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new {failure = true}, JsonRequestBehavior.AllowGet);
            }
        }
    }
}