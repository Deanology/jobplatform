﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using JobBoard.Models;
using JobBoard.Models.ViewModels;
using JobBoard.Services.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using static System.Web.HttpContext;

namespace JobBoard.Services.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationRoleManager _roleManager;

        public RoleRepository()
        {
            _context = Current.GetOwinContext().Get<ApplicationDbContext>();
            _userManager = Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            _roleManager = Current.GetOwinContext().Get<ApplicationRoleManager>();
        }

        public IEnumerable<ApplicationRole> GetAllRoles()
        {
            return _roleManager.Roles.ToList();
        }

        public void DeleteRole(string roleId)
        {
            var role = _roleManager.FindById(roleId);
            if (role != null) _roleManager.Delete(role);
        }

        public async Task<IdentityResult> AddRole(ApplicationRole role)
        {
            IdentityResult result = await _roleManager.CreateAsync(role);
            return result;
        }

        public IdentityResult AddUserToRole(string userId, string roleId)
        {
            var role = _roleManager.FindByName(roleId);
            var result = _userManager.AddToRoles(userId, role.Name);
            return result;
        }
        public IdentityResult RemoveUserFromRole(string userId, string rolename)
        {
            var role = _roleManager.FindByName(rolename);
            var result = _userManager.RemoveFromRole(userId, rolename);
            return result;
        }

        public async Task<IdentityResult> EditRole(string oldname, string newname)
        {
            var role = await _roleManager.FindByNameAsync(oldname);
            role.Name = newname;
            var result = await _roleManager.UpdateAsync(role);
            return result;
        }

        public async Task<ICollection<string>> GetRoleUsersId(string rolename)
        {
            var role = await _roleManager.FindByNameAsync(rolename);
            var roleUsers = role.Users;
            var usersId = roleUsers.Select(x => x.UserId).ToList();
            return usersId;
        }
        public async Task<IdentityRole> FindRoleByIdAsync(string id)
        {
            return await _roleManager.FindByIdAsync(id);
        }

        public async Task<IdentityRole> FindRoleByNameAsync(string rolename)
        {
            return await _roleManager.FindByNameAsync(rolename);
        }
    }
}