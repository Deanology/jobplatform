﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using JobBoard.Models;
using JobBoard.Models.ViewModels;
using JobBoard.Services.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using static System.Web.HttpContext;

namespace JobBoard.Services.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationRoleManager _roleManager;

        public AdminRepository()
        {
            //uses the system.web import
            _context = Current.GetOwinContext().Get<ApplicationDbContext>();
            _userManager = Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            _roleManager = Current.GetOwinContext().Get<ApplicationRoleManager>();
        }
        public IEnumerable<Category> GetAll()
        {
            return _context.Categories.ToList();
        }
        public void AddCategory(Category model)
        {
            model.CreatedAt = DateTime.Now;
            model.UpdatedAt = DateTime.Now;
            _context.Categories.Add(model);
            _context.SaveChanges();
        }

        public void UpdateCategory(Category model)
        {
            _context.Entry(model).State = EntityState.Modified;
            Save();
        }

        public async Task<Category> GetCategoryById(int id)
        {
            return _context.Categories.FirstOrDefault(x => x.Id == id);
        }
        public async Task Delete(int id)
        {
            var category = await GetCategoryById(id);
            _context.Categories.Remove(category);
            Save();
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}