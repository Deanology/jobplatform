﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobBoard.Models;
using Microsoft.AspNet.Identity;

namespace JobBoard.Services.Interfaces
{
    public interface IRoleRepository
    {
        IEnumerable<ApplicationRole> GetAllRoles();
        Task<IdentityResult> AddRole(ApplicationRole role);
    }
}
