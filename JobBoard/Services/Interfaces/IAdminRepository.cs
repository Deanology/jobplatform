﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JobBoard.Models;
using JobBoard.Models.ViewModels;

namespace JobBoard.Services.Interfaces
{
    public interface IAdminRepository
    {
        IEnumerable<Category> GetAll();
        Task<Category> GetCategoryById(int id);
        void AddCategory(Category model);
        void UpdateCategory(Category model);
        Task Delete(int id);
        void Save();
    }
}
