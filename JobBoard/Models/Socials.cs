﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JobBoard.Models
{
    public class Socials
    {
        [Display(Name = "Facebook URL")]
        public string FacebookURL { get; set; }
        [Display(Name = "Google+ URL")]
        public string GoogleURL { get; set; }
        [Display(Name = "LinkedIn URL")]
        public string LinkedInURL { get; set; }
        [Display(Name = "Twitter URL")]
        public string TwitterURL { get; set; }
        [Display(Name = "Instagram URL")]
        public string InstagramURL { get; set; }
        [Display(Name = "Pinterest URL")]
        public string PinterestURL { get; set; }
    }
}