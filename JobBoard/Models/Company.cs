﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JobBoard.Models.Enum;

namespace JobBoard.Models
{
    public class Company
    {
        public string CompanyName { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyWebsiteURL { get; set; }
        public string CompanyPhoneNumber { get; set; }
        public string CompanyDescription { get; set; }
        public  DateTime EstablishedDate { get; set; }
        public string CompanySize { get; set; }
        public int Branches { get; set; }
        public CompanyType CompanyType { get; set; }
    }
}