﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JobBoard.Models.ViewModels
{
    public class ApplicationRoleViewModel
    {
        [DisplayName("Role Name:")]
        [Required(ErrorMessage = "Please enter the role name.")]
        public string RoleName { get; set; }
        [DisplayName("Role Description:")] 
        public string RoleDesription { get; set; }
    }
}