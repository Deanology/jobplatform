﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JobBoard.Models.Enum
{
    public enum JobTypes
    {
        [Display(Name = "Full Time")]
        FullTime,
        [Display(Name = "Part Time")]
        PartTime,
        [Display(Name = "Freelancing")]
        Freelancing,
        [Display(Name = "Internship")]
        Internship,
        [Display(Name = "Contract")]
        Contract
    }
}