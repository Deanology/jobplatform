﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JobBoard.Models.Enum
{
    public enum Qualification
    {
        [Display(Name = "Certificate")]
        Certificate,
        [Display(Name = "Diploma")]
        Diploma,
        [Display(Name = "Bachelor Degree")]
        BachelorDegree,
        [Display(Name = "Master Degree")]
        MasterDegree,
        [Display(Name = "Post Graduate")]
        PostGraduate
    }
}