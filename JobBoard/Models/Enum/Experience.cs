﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JobBoard.Models.Enum
{
    public enum Experience
    {
        [Display(Name = "Entry Level")]
        EntryLevel,
        [Display(Name = "Less Than One Year")]
        LessThanOneYear,
        [Display(Name = "2 Years")]
        Year2,
        [Display(Name = "3 Years")]
        Year3,
        [Display(Name = "4 Years")]
        Year4,
        [Display(Name = "5+ Years")]
        Year5
    }
}