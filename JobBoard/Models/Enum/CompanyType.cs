﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JobBoard.Models.Enum
{
    public enum CompanyType
    {
        [Display(Name = "Public Limited")]
        PublicLimited,
        [Display(Name = "Private Limited")]
        PrivateLimited,
        [Display(Name = "C Corporation")]
        CCoporations,
        [Display(Name = "S Corporation")]
        SCoporations
    }
}