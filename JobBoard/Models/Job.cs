﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JobBoard.Models.Enum;

namespace JobBoard.Models
{
    public class Job
    {
        public int Id { get; set; }
        public string JobTitle { get; set; }
        public int Vacancies { get; set; }
        public int Views { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string JobOverview { get; set; }
        public string SkillsAndQualification { get; set; }
        public string Education { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int AddressId { get; set; }
        public Address Address { get; set; }
        public JobTypes JobTypes { get; set; }
        public Gender Gender { get; set; }
        public  Experience Experience { get; set; }
        public OfferedSalary OfferedSalary { get; set; }
        public Qualification Qualification { get; set; }
    }
}